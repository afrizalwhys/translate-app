import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:translator/translator.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Translator App English to Indonesia',
      home:
          const MyHomePage(title: 'Translator App English to Indonesia'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

TextEditingController textController = new TextEditingController();

var translatedPhrase = "";

var translator = GoogleTranslator();

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Container(
                width: double.infinity,
                height: double.infinity,
                color: Colors.blue,
                child: Column(
                  children: [
                    TextField(
                      controller: textController,
                    ),
                    MaterialButton(
                        child: Text('Translate'),
                        color: Colors.white,
                        onPressed: () {
                          setState(() {
                            translator
                                .translate(textController.text,
                                    from: "en", to: "id")
                                .then((t) {
                              setState(() {
                                translatedPhrase = t.toString();
                              });
                            });
                          });
                        }),
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(
                width: double.infinity,
                height: double.infinity,
                color: Colors.purple,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      translatedPhrase,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MaterialButton(
                            child: Icon(Icons.clear),
                            onPressed: () {
                              setState(() {
                                translatedPhrase = "";
                                textController.text = "";
                              });
                            }),
                        MaterialButton(
                            child: Icon(Icons.content_copy),
                            onPressed: () {
                              Clipboard.setData(
                                  new ClipboardData(text: translatedPhrase));
                            }),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
